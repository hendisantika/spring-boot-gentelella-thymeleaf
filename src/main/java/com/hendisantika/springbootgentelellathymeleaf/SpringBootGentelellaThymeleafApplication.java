package com.hendisantika.springbootgentelellathymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGentelellaThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootGentelellaThymeleafApplication.class, args);
    }
}
