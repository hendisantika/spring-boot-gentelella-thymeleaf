# spring-boot-gentelella-thymeleaf

Integrasi admin template https://github.com/puikinsh/gentelella dengan spring boot dan thymeleaf.

Untuk menjalankan di local, ketikkan:

`mvn clean spring-boot:run`

## Screen shot

Home

![Home](img/index.png "Home")

Index 1

![Index 1](img/index1.png "Index 1 Home Page")

Index 2

![Index 2](img/index2.png "Index 2 Home Page")

Index 3

![Index 3](img/index3.png "Index 3 Home Page")

General Form

![General Form](img/general_form.png "General Form")

Advanced Form

![Advanced Form](img/advance_form.png "Advanced Form")